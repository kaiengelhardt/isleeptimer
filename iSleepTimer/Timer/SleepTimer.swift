import Cocoa

class SleepTimer {
    
    enum SleepTimerState {
        case Idle
        case Running
        case Paused
    }
    
    private let DefaultTimerInterval = 0.5
    var timer: Timer!
    
    var duration: NSTimeInterval
    var elapsedTime: NSTimeInterval = 0.0
    
    let action: Double -> ()
    let completion: () -> ()
    
    private(set) var state: SleepTimerState = .Idle
    
    private var lastTime: NSTimeInterval = 0.0
    
    init(duration: NSTimeInterval, action: Double -> (), completion: () -> ()) {
        assert(duration > 0.0, "Duration should be positive!")
        
        self.duration = duration
        self.action = action
        self.completion = completion
        
        timer = Timer(timeInterval: DefaultTimerInterval, action: timerFired, startImmediately: false)
    }
    
    func start() {
        if state == .Idle {
            elapsedTime = 0.0
            timer.start()
            lastTime = CFAbsoluteTimeGetCurrent()
            state = .Running
        }
    }
    
    func pause() {
        if state == .Running {
            timer.stop()
            state = .Paused
        }
    }
    
    func resume() {
        if state == .Paused {
            timer.start()
            lastTime = CFAbsoluteTimeGetCurrent()
            state = .Running
        }
    }
    
    
    func stop() {
        if state != .Idle {
            timer.stop()
            state = .Idle
        }
    }
    
    private func timerFired() {
        let delta = CFAbsoluteTimeGetCurrent() - lastTime
        lastTime = CFAbsoluteTimeGetCurrent()
        elapsedTime += delta
        
        if elapsedTime < duration {
            let progress = elapsedTime / duration
            action(progress)
        } else {
            completion()
            stop()
        }
    }
    
}
