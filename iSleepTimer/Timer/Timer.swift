import Cocoa

class Timer {
    
    /// The state of the timer.
    enum TimerState {
        /// The timer is stopped.
        case Idle
        /// The timer is running.
        case Running
    }
    
    /// The interval, in which the action is repeated.
    let timeInterval: NSTimeInterval
    /// The action which shall be called repeatedly.
    let action: () -> ()
    
    private var timer: NSTimer?
    
    private var state: TimerState = .Idle
    
    /// Creates a new Timer object.
    ///
    /// :param: `timeInterval` The interval, in seconds, in which the passed in action should be repeated.
    /// :param: `action` This block is called repeatedly in the specified interval.
    /// :param: `startImmediately` 
    init(timeInterval: NSTimeInterval, action: () -> (), startImmediately: Bool) {
        self.timeInterval = timeInterval
        self.action = action
        
        if startImmediately {
            start()
        }
    }
    
    /// Starts the timer, if it is not already running.
    func start() {
        if state == .Idle {
            timer = NSTimer(timeInterval: timeInterval, target: self, selector: "timerFired", userInfo: nil, repeats: true)
            state = .Running
        }
    }
    
    /// Stops the timer, if it is not already stopped.
    func stop() {
        if state == .Running {
            timer?.invalidate()
            timer = nil
            state = .Idle
        }
    }
    
    private func timerFired() {
        action()
    }
    
}
