import Cocoa

class SleepTimerViewController: NSViewController {

    @IBOutlet weak var startStopButton: NSButton!
    @IBOutlet weak var profileButton: NSButton!
    
    @IBOutlet weak var timeLabel: NSTextField!
    @IBOutlet weak var progressBar: NSProgressIndicator!
    
    @IBOutlet weak var timeStepper: NSStepper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Actions
    
    @IBAction func toggleSleepTimer(sender: NSButton) {
    }
    
    @IBAction func timeStepperValueChanged(sender: NSStepper) {
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: NSStoryboardSegue, sender: AnyObject?) {
    }
    
}
